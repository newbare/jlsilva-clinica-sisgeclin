package br.com.clinica.entity;


// Generated 26/03/2011 00:27:00 by Hibernate Tools 3.2.4.GA

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;

/**
 * Clientes generated by hbm2java
 */
@Entity
@Table(name = "clientes", schema = "public")
public class Clientes implements java.io.Serializable {

	private static final long serialVersionUID = -5669697435831787701L;
	private int codclientes;
	private String nome;
	private String cpf;
	private String rg;
	private String sexo;
	private Date dtnasc;
	private String endereco;
	private String profissao;
	private String complemento;
	private String cidade;
	private String estado;
	private String cep;
	private String email;
	private String bairro;	
	private String contato1;
	private String telefone1;
	private String contato2;
	private String telefone2;	
	private String numero;	
	private Set<Clientestel> clientestels = new HashSet<Clientestel>(0);
	private Set<Animal> animals = new HashSet<Animal>(0);

	public Clientes() {
	}

	public Clientes(int codclientes, String nome, String email,
			boolean tipopessoa) {
		this.codclientes = codclientes;
		this.nome = nome;
		this.email = email;
		
	}

	public Clientes(int codclientes, String nome, String cpf, String rg,
			String sexo, Date dtnasc, String endereco, String profissao,
			String complemento, String cidade, String estado, String cep,
			String email, String bairro, String ultimoemail, String contato1,
			String telefone1, String contato2, String telefone2,
			boolean tipopessoa, String cnpj, String ie, String numero,
			String fantasia, Set<Clientestel> clientestels, Set<Animal> animals) {
		this.codclientes = codclientes;
		this.nome = nome;
		this.cpf = cpf;
		this.rg = rg;
		this.sexo = sexo;
		this.dtnasc = dtnasc;
		this.endereco = endereco;
		this.profissao = profissao;
		this.complemento = complemento;
		this.cidade = cidade;
		this.estado = estado;
		this.cep = cep;
		this.email = email;
		this.bairro = bairro;
		//this.ultimoemail = ultimoemail;
		this.contato1 = contato1;
		this.telefone1 = telefone1;
		this.contato2 = contato2;
		this.telefone2 = telefone2;
		//this.tipopessoa = tipopessoa;
		//this.cnpj = cnpj;
		//this.ie = ie;
		this.numero = numero;
		//this.fantasia = fantasia;
		this.clientestels = clientestels;
		this.animals = animals;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "id_cli")
	@SequenceGenerator( name = "id_cli", sequenceName = "id_cli")
	@Column(name = "codclientes", unique = true, nullable = false)
	public int getCodclientes() {
		return this.codclientes;
	}

	public void setCodclientes(int codclientes) {
		this.codclientes = codclientes;
	}

	@Column(name = "nome", nullable = false, length = 50)
	@NotNull
	@Length(max = 50)
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Column(name = "cpf", length = 20)
	@Length(max = 20)
	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Column(name = "rg", length = 20)
	@Length(max = 20)
	public String getRg() {
		return this.rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	@Column(name = "sexo", length = 50)
	@Length(max = 50)
	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dtnasc", length = 13)
	public Date getDtnasc() {
		return this.dtnasc;
	}

	public void setDtnasc(Date dtnasc) {
		this.dtnasc = dtnasc;
	}

	@Column(name = "endereco", length = 70)
	@Length(max = 70)
	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	@Column(name = "profissao", length = 50)
	@Length(max = 50)
	public String getProfissao() {
		return this.profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	@Column(name = "complemento", length = 50)
	@Length(max = 50)
	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	@Column(name = "cidade", length = 25)
	@Length(max = 25)
	public String getCidade() {
		return this.cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@Column(name = "estado", length = 2)
	@Length(max = 2)
	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name = "cep", length = 11)
	@Length(max = 11)
	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Column(name = "email", nullable = false, length = 50)
	@NotNull
	@Length(max = 50)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "bairro", length = 50)
	@Length(max = 50)
	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	

	@Column(name = "contato1", length = 30)
	@Length(max = 30)
	public String getContato1() {
		return this.contato1;
	}

	public void setContato1(String contato1) {
		this.contato1 = contato1;
	}

	@Column(name = "telefone1", length = 20)
	@Length(max = 20)
	public String getTelefone1() {
		return this.telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	@Column(name = "contato2", length = 30)
	@Length(max = 30)
	public String getContato2() {
		return this.contato2;
	}

	public void setContato2(String contato2) {
		this.contato2 = contato2;
	}

	@Column(name = "telefone2", length = 20)
	@Length(max = 20)
	public String getTelefone2() {
		return this.telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
		}	

	@Column(name = "numero", length = 50)
	@Length(max = 50)
	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}	

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "clientes")
	public Set<Clientestel> getClientestels() {
		return this.clientestels;
	}

	public void setClientestels(Set<Clientestel> clientestels) {
		this.clientestels = clientestels;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "clientes")
	public Set<Animal> getAnimals() {
		return this.animals;
	}

	public void setAnimals(Set<Animal> animals) {
		this.animals = animals;
	}

}
